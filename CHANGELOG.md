## FediPhoto-Lineage Changelog

#### v4.0 Thursday 4 November 2021

Lots of improvements and bugfixes in this milestone release:

* Fallback to first account added when deleting the (2nd or more) active account (#29)
* Fallback to first status config added when deleting the (2nd or more) active status config
* App will now mandate you to create a status config if there is no one (#37)
* Mitigation of the crashes when entering a wrong fediverse instance domain name (#35)
* Changed app description to warn if someone not using the standard android camera app (#41)

#### v3.9 Thursday 14 October 2021

Small improvements and bugfixes in this release:

* Fixed app name and updated screenshots for F-Droid (#15 and #17)
* Info button in menu now points to our page (#24)
* Location is no more posted if GPS location format is blank (#30)
* Solved some crashy situations
* Other minor bugfixes (#18 and others not even listed)


#### v3.8 Tuesday 5 October 2021

Big rework under the hood! If you have installed a version before this you need to uninstall it.
* Implemented a proper db instead of json files (issue #13)

Fixes:
* Fix for Media Storage fix not working on language different from en-us (issue #14)
* Some fixes to make clearer to add status configuration and accounts (issues #2 and #10)
* Account-Status configuration not shown (issue #8)
* Profile set as active even without check the mark (issue #11)
* Photos posted with wrong account (issue #7)

#### v3.7 Tuesday 28 September 2021

* Fixed the storage issues on Android 10+

* Changed user agent

#### v3.6 Monday 27 September 2021

* First release of this fork!

* Changed logos, colours and package names.

* Updated some libraries versions.



## FediPhoto Changelog

#### v3.5 Saturday November 7, 2020

* Do not set content-length on POST. The length was incorrect when unicode characters are present. Length is set by the API anyway. <a href='https://github.com/pla1/FediPhoto/issues/18'>Issue 18</a>. Emoji characters in status configs work now.

#### v3.4 Monday September 7, 2020

* Changed default Open Street Map URL to avoid redirects which appear to cause the <a href='https://git.pleroma.social/pleroma/pleroma/-/issues/2119'>Pleroma preview method to choose random languages.</a>

* Added Status Config menu option to test the date format.

* Added Status Config menu option to test the GPS coordinates format.

* Changed the Status text field to be multiline.

* Wrapped the Config Status view in scroll view in case the status text is long and using a low resolution device.

* Bumped targetSdkVersion from 28 to 29.

* Bumped dependencies versions to the latest for constraintlayout, runner, and espresso-core.

* Removed key listener on the date format. Use the new Status Config menu option to test the date format.

#### v3.3 Saturday August 15, 2020

* gl & es strings localized by <a href='https://github.com/xmgz'>xmgz</a>.

#### v3.2 Saturday August 15, 2020

* Added threading. <a href='https://github.com/pla1/FediPhoto/issues/14'>Issue 14</a>

* Added padding around text. <a href='https://github.com/pla1/FediPhoto/issues/15'>Issue 15</a>

#### v3.1 Friday July 3, 2020

* Replaced icon with new design by <a href="https://github.com/silkevicious">Sylke Vicious</a>.

* Updated Italian translation by <a href="https://github.com/silkevicious">Sylke Vicious</a>.

#### v3.0 Wednesday July 1, 2020

* Allow app to be installed on external storage.

* Few more strings localized by <a href='https://github.com/xmgz'>xmgz</a>.

#### v2.9 Monday June 22, 2020

* Added missing entries from strings.xml

#### v2.8 Thursday June 18, 2020

* Added Euskera localization by <a href="https://pleroma.libretux.com/users/izaro">Izaro</a>

#### v2.7 Wednesday June 17, 2020

* Added Italian localization by <a href="https://github.com/silkevicious">Sylke Vicious</a>

* Added Catalan localization by <a href="https://github.com/splacat">spla</a>

#### v2.6 Sunday June 14, 2020

* Fixed posting to PixelFed.

* Fixed bugs relating to visibility.

* Added prompt to delete workers if they are over a day old.

#### v2.5 Saturday June 13, 2020

* Forgot to commit version bump for tagged release.

#### v2.4 June 13, 2020

* Added Galician and Spanish localization by <a href="https://github.com/xmgz">xmgz</a>.

* README updated

#### v2.2 June 5, 2020

* Use Locale.US when formatting GPS coordinates into the map URL. This will prevent some languages from using a comma instead of a period for the decimal place.

#### v2.1 November 29, 2019

* Changed the default mapping URL from Google Maps to Open Street Map (OSM).

#### v2.0 September 15, 2019

* Add location permission to manifest. Needed to get the GPS coordinates from the photo Exif interface.

#### v1.9 September 15, 2019

* Changed string format for GPS coordinates to float so you can control the number of decimal places.

#### v1.8 August 8, 2019

* Fix visibility of worker classes.

#### v1.7 August 18, 2019

* Fixed some code issues that the inspection tool listed.

* Added About menu item that sends the user to <a href="https://fediphoto.com">https://fediphoto.com</a>

* Changed photo file name prefix from Fedi_Photo_ to just photo_.

* Send notification before the photo is moved, copied, or deleted.

#### v1.6 August 8, 2019

* Added fastlane directory structure to assist F-Droid builds.

#### v1.5 August 6, 2019

* Handle deleting last account and last status without crashing app.

#### v1.5a August 4, 2019

* Add notifications

* Use WorkManager chaining to ensure photo is uploaded and post is made regardless of network conditions
