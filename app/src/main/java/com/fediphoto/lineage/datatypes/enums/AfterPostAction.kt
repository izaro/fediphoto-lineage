package com.fediphoto.lineage.datatypes.enums

enum class AfterPostAction {
    LEAVE,
    COPY,
    MOVE,
    DELETE
}
