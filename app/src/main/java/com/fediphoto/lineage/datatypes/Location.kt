package com.fediphoto.lineage.datatypes

data class Location(
    val latitude: Float,
    val longitude: Float
)
