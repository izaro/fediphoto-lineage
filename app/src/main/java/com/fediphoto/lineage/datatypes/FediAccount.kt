package com.fediphoto.lineage.datatypes

data class FediAccount(
    val id: String,
    val username: String,
    val acct: String,
    val displayName: String,
    val url: String,
    val avatar: String
)
