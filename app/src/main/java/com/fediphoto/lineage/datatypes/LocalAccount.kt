package com.fediphoto.lineage.datatypes

data class LocalAccount(
    val id: Int,
    val instance: String,
    val username: String,
    val userUrl: String,
    val token: String,
    val displayName: String,
    val avatarUrl: String
)
