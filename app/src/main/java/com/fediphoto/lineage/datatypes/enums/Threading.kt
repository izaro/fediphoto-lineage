package com.fediphoto.lineage.datatypes.enums

enum class Threading {
    NEVER,
    DAILY,
    ALWAYS
}
