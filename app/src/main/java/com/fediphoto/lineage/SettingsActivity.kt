package com.fediphoto.lineage

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.Adapter
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.fediphoto.lineage.databinding.ActivitySettingsBinding
import com.fediphoto.lineage.datatypes.enums.AfterPostAction

class SettingsActivity : Activity() {
    private lateinit var viewBinding: ActivitySettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivitySettingsBinding.inflate(layoutInflater)
        val prefs = Prefs(this)

        val afterPostActionList = mutableListOf<String>()
        for (i in AfterPostAction.values().indices) afterPostActionList.add(resources.getStringArray(R.array.actionAfterPostEntries)[i])
        viewBinding.afterPostAction.adapter =
            ArrayAdapter(this, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, afterPostActionList)
        viewBinding.afterPostAction.setSelection(prefs.afterPostAction.ordinal)
        viewBinding.afterPostAction.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                prefs.afterPostAction = AfterPostAction.values()[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        viewBinding.cameraOnStartCheckbox.isChecked = prefs.cameraOnStart
        viewBinding.cameraOnStart.setOnClickListener { viewBinding.cameraOnStartCheckbox.isChecked = !viewBinding.cameraOnStartCheckbox.isChecked }
        viewBinding.cameraOnStartCheckbox.setOnCheckedChangeListener { _, isChecked -> prefs.cameraOnStart = isChecked }
        setContentView(viewBinding.root)
    }
}
