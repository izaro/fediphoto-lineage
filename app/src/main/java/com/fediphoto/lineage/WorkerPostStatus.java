package com.fediphoto.lineage;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.fediphoto.lineage.datatypes.LocalAccount;
import com.fediphoto.lineage.datatypes.Template;
import com.fediphoto.lineage.datatypes.StatusThread;
import com.fediphoto.lineage.datatypes.enums.Threading;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

public class WorkerPostStatus extends Worker {

    private final String TAG = this.getClass().getCanonicalName();
    private final Context context;

    public WorkerPostStatus(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
        this.context = context;
    }


    @NonNull
    @Override
    public Result doWork() {
        Data dataInput = getInputData();
        String photoFileName = dataInput.getString(MainActivity.Literals.fileName.name());
        int accountId = dataInput.getInt(MainActivity.Literals.accountIndexSelected.name(), -1);
        int templateId = dataInput.getInt(MainActivity.Literals.statusIndexSelected.name(), -1);
        if (Utils.isBlank(photoFileName)) {
            Toast.makeText(context, R.string.photo_file_name_blank, Toast.LENGTH_LONG).show();
            return Result.failure();
        }
        Database database = new Database(context);
        JsonObject params = new JsonObject();
        Template template = database.getTemplateById(templateId);
        LocalAccount account = database.getAccountById(accountId);
        Log.i(TAG, String.format("Selected account from settings: %s", account));
        String instance = account.getInstance();
        String visibility = template.getVisibility().getUrlValue();
        String threading = template.getThreading().name();
        StringBuilder sb = new StringBuilder();
        sb.append(template.getText());
        File file = new File(photoFileName);
        if (!file.exists()) {
            Log.i(TAG, String.format("Photo file %s does not exist.", photoFileName));
            return Result.failure();
        }
        long photoFileLastModified = file.lastModified();
        double[] latLong;
        double latitude = 0;
        double longitude = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(file.getAbsolutePath());
            latLong = exifInterface.getLatLong();
            if (latLong != null) {
                latitude = latLong[0];
                longitude = latLong[1];
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean isDebuggable = (0 != (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));
        if (isDebuggable) {
            latitude = 19.677821;
            longitude = -155.596364;
            Log.i(TAG, String.format("Debug mode with hard coded lat and long %.3f %.3f", latitude, longitude));
        }
        Log.i(TAG, String.format("Latitude %f longitude %f", latitude, longitude));
        if (template.getLocation() && latitude != 0) {
            String gpsCoordinatesFormat = template.getLocationFormat();
            if (gpsCoordinatesFormat.split("%").length == 3) {
                sb.append("\n").append(String.format(Locale.US, gpsCoordinatesFormat, latitude, longitude));
            } else {
                sb.append("\n").append(latitude).append(",").append(longitude);
            }
        }
        String dateFormat = template.getDateFormat();
        if (template.getDate() && Utils.isNotBlank(dateFormat)) {
            if (photoFileLastModified != 0) {
                String dateDisplay = new SimpleDateFormat(dateFormat, Locale.US).format(new Date(photoFileLastModified));
                sb.append("\n").append(dateDisplay);
            }
        }
        params.addProperty(MainActivity.Literals.status.name(), sb.toString());
        params.addProperty(MainActivity.Literals.access_token.name(), account.getToken());
        params.addProperty(MainActivity.Literals.visibility.name(), visibility);
        JsonArray mediaJsonArray = new JsonArray();
        mediaJsonArray.add(dataInput.getString(MainActivity.Literals.id.name()));
        params.add(MainActivity.Literals.media_ids.name(), mediaJsonArray);
        params.addProperty(MainActivity.Literals.client_name.name(), context.getString(R.string.fedi_photo_for_android));
        StatusThread statusThread = database.getThread(accountId, templateId);
        if (statusThread.getStatusId().length() != 0) {
            if ((template.getThreading() == Threading.DAILY && Utils.getDateYyyymmdd().equals(statusThread.getStatusDate()))
                    || template.getThreading() == Threading.ALWAYS) {
                params.addProperty(MainActivity.Literals.in_reply_to_id.name(), statusThread.getStatusId());
                Log.i(TAG, String.format("%s threading ID: %s set to in_reply_to_id.", threading, statusThread.getStatusId()));
            }
        }
        String urlString = String.format("https://%s/api/v1/statuses", instance);
        Log.i(TAG, "URL " + urlString);
        HttpsURLConnection urlConnection;
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            URL url = new URL(urlString);
            urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Cache-Control", "no-cache");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("User-Agent", "FediPhoto-Lineage");
            urlConnection.setUseCaches(false);
            urlConnection.setRequestMethod(MainActivity.Literals.POST.name());
            urlConnection.setRequestProperty("Content-type", "application/json; charset=UTF-8");
            urlConnection.setDoOutput(true);
            String token = account.getToken();
            String authorization = String.format("Bearer %s", token);
            urlConnection.setRequestProperty("Authorization", authorization);
            String json = params.toString();
            Log.i(TAG, String.format("Posting JSON: %s", json));
       //     urlConnection.setRequestProperty("Content-length", Integer.toString(json.length()));
            outputStream = urlConnection.getOutputStream();
            outputStream.write(json.getBytes());
            outputStream.flush();
            int responseCode = urlConnection.getResponseCode();
            Log.i(TAG, String.format("Response code: %d\n", responseCode));
            urlConnection.setInstanceFollowRedirects(true);
            inputStream = urlConnection.getInputStream();
            InputStreamReader isr = new InputStreamReader(inputStream);
            Gson gson = new Gson();
            JsonObject jsonObjectFromPost = gson.fromJson(isr, JsonObject.class);
            Utils.log(TAG, String.format("Output: %s", jsonObjectFromPost.toString()));
            String urlForPost = Utils.getProperty(jsonObjectFromPost, MainActivity.Literals.url.name());
            Data dataOutput = new Data.Builder()
                    .putString(MainActivity.Literals.url.name(), urlForPost)
                    .build();
            sendNotification(context.getString(R.string.post_success), urlForPost, photoFileName);
            mediaActionAfterPost(dataInput);
            threadingMaintenanceAfterPost(jsonObjectFromPost);
            return Result.success(dataOutput);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.retry();
        } finally {
            Utils.close(inputStream, outputStream);
        }
    }

    private void threadingMaintenanceAfterPost(JsonObject jsonObjectFromPost) {
        Data dataInput = getInputData();
        int accountId = dataInput.getInt(MainActivity.Literals.accountIndexSelected.name(), -1);
        int templateId = dataInput.getInt(MainActivity.Literals.statusIndexSelected.name(), -1);
        Database database = new Database(context);
        Template template = database.getTemplateById(templateId);
        String id = Utils.getProperty(jsonObjectFromPost, MainActivity.Literals.id.name());
        Log.i(TAG, String.format("Threading: %s ID of post: %s", template.getThreading().name(), id));
        StatusThread statusThread = database.getThread(accountId, templateId);
        if (template.getThreading() == Threading.ALWAYS) {
            database.setThreadingId(accountId, templateId, id, Utils.getDateYyyymmdd());
        }
        if (template.getThreading() == Threading.DAILY) {
            if (statusThread.getStatusId().length() == 0) {
                database.setThreadingId(accountId, templateId, id, Utils.getDateYyyymmdd());
            } else {
                if (!Utils.getDateYyyymmdd().equals(statusThread.getStatusDate())) {
                    database.setThreadingId(accountId,templateId, id, Utils.getDateYyyymmdd());
                }
            }
        }
        if (template.getThreading() == Threading.NEVER) {
            database.clearThreading(templateId);
        }
    }

    private void mediaActionAfterPost(Data data) {
        String fileName = data.getString(MainActivity.Literals.fileName.name());
        if (Utils.isBlank(fileName)) {
            Log.i(TAG, "File name if blank. No action after post taken.");
            return;
        }
        File file = new File(fileName);
        if (!file.exists()) {
            Log.i(TAG, String.format("File %s not found. No action after post taken.", fileName));
            return;
        }
        Prefs prefs = new Prefs(context);
        Log.i(TAG, String.format("Action after post \"%s\" on file %s", prefs.getAfterPostAction(), fileName));
        boolean fileDeleted;
        switch (prefs.getAfterPostAction()) {
            case LEAVE:
                Log.i(TAG, String.format("File %s left in place.", fileName));
                break;
            case COPY:
                saveToExternal(file);
                Log.i(TAG, String.format("File %s copied to external storage.", fileName));
                break;
            case MOVE:
                saveToExternal(file);
                fileDeleted = file.delete();
                Log.i(TAG, String.format("File %s copied to external storage and deleted %s.", fileName, fileDeleted));
                break;
            case DELETE:
                fileDeleted = file.delete();
                Log.i(TAG, String.format("File %s deleted %s", fileName, fileDeleted));
                break;
        }
    }

    private void saveToExternal(File sourceFile) {
        try {
            if (!sourceFile.exists()) { return; }

            FileChannel source = new FileInputStream(sourceFile).getChannel();

            OutputStream fos;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                ContentResolver resolver = context.getContentResolver();
                ContentValues contentValues = new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, sourceFile.getName() + ".jpg");
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES);
                Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));
            } else {
                String imagesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
                File image = new File(imagesDir, sourceFile.getName() + ".jpg");
                fos = new FileOutputStream(image);
            }

            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = source.read(ByteBuffer.wrap(buffer))) != -1) {
                Objects.requireNonNull(fos).write(buffer, 0, bytesRead);
            }

            Objects.requireNonNull(fos).close();
            source.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void sendNotification(String title, String urlString, String photoFileName) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(urlString));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default", "Default", NotificationManager.IMPORTANCE_DEFAULT);
            Objects.requireNonNull(notificationManager).createNotificationChannel(channel);
        }

        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(), "default")
                .setContentTitle(title)
                .setContentText(urlString)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.fediphoto_foreground)
                .setLargeIcon(BitmapFactory.decodeFile(photoFileName))
                .setAutoCancel(true);
        Random random = new Random();
        int id = random.nextInt(9999 - 1000) + 1000;
        Objects.requireNonNull(notificationManager).notify(id, notification.build());
    }

}
