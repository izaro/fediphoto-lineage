package com.fediphoto.lineage

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.LocalAccount
import com.fediphoto.lineage.datatypes.Template
import com.fediphoto.lineage.datatypes.StatusThread
import com.fediphoto.lineage.datatypes.enums.Threading
import com.fediphoto.lineage.datatypes.enums.Visibility

class Database(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
    companion object {
        private const val DB_NAME = "data.sqlite"
        private const val DB_VERSION = 1

        private const val ROW_ID = "rowid"

        private const val TABLE_ACCOUNTS = "accounts"
        private const val ACCOUNTS_COLUMN_AVATAR_URL = "avatar_url"
        private const val ACCOUNTS_COLUMN_USERNAME = "username"
        private const val ACCOUNTS_COLUMN_USER_URL = "url"
        private const val ACCOUNTS_COLUMN_DISPLAY_NAME = "display_name"
        private const val ACCOUNTS_COLUMN_INSTANCE = "instance"
        private const val ACCOUNTS_COLUMN_TOKEN = "token"

        private const val TABLE_TEMPLATES = "templates"
        private const val TEMPLATES_COLUMN_NAME = "name"
        private const val TEMPLATES_COLUMN_TEXT = "text"
        private const val TEMPLATES_COLUMN_VISIBILITY = "visibility"
        private const val TEMPLATES_COLUMN_THREADING = "threading"
        private const val TEMPLATES_COLUMN_DATE = "date"
        private const val TEMPLATES_COLUMN_DATE_FORMAT = "date_format"
        private const val TEMPLATES_COLUMN_LOCATION = "location"
        private const val TEMPLATES_COLUMN_LOCATION_FORMAT = "location_format"

        private const val TABLE_THREADING = "threading"
        private const val THREADING_COLUMN_ACCOUNT_ID = "accountId"
        private const val THREADING_COLUMN_TEMPLATE_ID = "templateId"
        private const val THREADING_COLUMN_LAST_STATUS_ID = "last_status_id"
        private const val THREADING_COLUMN_LAST_STATUS_DATE = "last_status_date"

        private const val TABLE_QUEUE = "queue"
        private const val QUEUE_COLUMN_ACCOUNT_ID = "account_id"
        private const val QUEUE_COLUMN_IMAGE_PATH = "image_path"
        private const val QUEUE_COLUMN_CONTENT = "content"
        private const val QUEUE_COLUMN_VISIBILITY = "visibility"
        private const val QUEUE_COLUMN_TEMPLATE_ID = "template_id"
        private const val QUEUE_COLUMN_THREADING = "threading"
        private const val QUEUE_COLUMN_STATE = "state"
        private const val QUEUE_COLUMN_ERROR = "error"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(
            "CREATE TABLE $TABLE_ACCOUNTS(" +
                    "$ACCOUNTS_COLUMN_INSTANCE TEXT," +
                    "$ACCOUNTS_COLUMN_USERNAME TEXT," +
                    "$ACCOUNTS_COLUMN_TOKEN TEXT," +
                    "$ACCOUNTS_COLUMN_USER_URL TEXT," +
                    "$ACCOUNTS_COLUMN_DISPLAY_NAME TEXT," +
                    "$ACCOUNTS_COLUMN_AVATAR_URL TEXT," +
                    "PRIMARY KEY($ACCOUNTS_COLUMN_INSTANCE, $ACCOUNTS_COLUMN_USERNAME)" +
                    ")"
        )
        db?.execSQL(
            "CREATE TABLE $TABLE_TEMPLATES(" +
                    "$TEMPLATES_COLUMN_NAME TEXT," +
                    "$TEMPLATES_COLUMN_TEXT TEXT," +
                    "$TEMPLATES_COLUMN_VISIBILITY INT," +
                    "$TEMPLATES_COLUMN_THREADING INT," +
                    "$TEMPLATES_COLUMN_DATE INT," +
                    "$TEMPLATES_COLUMN_DATE_FORMAT TEXT," +
                    "$TEMPLATES_COLUMN_LOCATION INT," +
                    "$TEMPLATES_COLUMN_LOCATION_FORMAT TEXT" +
                    ")"
        )
        db?.execSQL(
            "CREATE TABLE $TABLE_THREADING(" +
                    "$THREADING_COLUMN_ACCOUNT_ID TEXT," +
                    "$THREADING_COLUMN_TEMPLATE_ID TEXT," +
                    "$THREADING_COLUMN_LAST_STATUS_ID TEXT," +
                    "$THREADING_COLUMN_LAST_STATUS_DATE TEXT," +
                    "PRIMARY KEY($THREADING_COLUMN_ACCOUNT_ID, $THREADING_COLUMN_TEMPLATE_ID)" +
                    ")"
        )
        db?.execSQL(
            "CREATE TABLE $TABLE_QUEUE(" +
                    "$QUEUE_COLUMN_ACCOUNT_ID INT," +
                    "$QUEUE_COLUMN_IMAGE_PATH TEXT," +
                    "$QUEUE_COLUMN_CONTENT TEXT," +
                    "$QUEUE_COLUMN_VISIBILITY INT," +
                    "$QUEUE_COLUMN_TEMPLATE_ID INT," +
                    "$QUEUE_COLUMN_THREADING INT," +
                    "$QUEUE_COLUMN_STATE INT," +
                    "$QUEUE_COLUMN_ERROR TEXT" +
                    ")"
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {}

    fun addAccount(fediAccount: FediAccount, instance: String, token: String, onResult: (accountId: Int) -> Unit) {
        writableDatabase.apply {
            val result = insertWithOnConflict(
                TABLE_ACCOUNTS,
                null,
                ContentValues().apply {
                    put(ACCOUNTS_COLUMN_INSTANCE, instance)
                    put(ACCOUNTS_COLUMN_USERNAME, fediAccount.username)
                    put(ACCOUNTS_COLUMN_TOKEN, token)
                    put(ACCOUNTS_COLUMN_USER_URL, fediAccount.url)
                    put(ACCOUNTS_COLUMN_DISPLAY_NAME, fediAccount.displayName)
                    put(ACCOUNTS_COLUMN_AVATAR_URL, fediAccount.avatar)
                },
                SQLiteDatabase.CONFLICT_REPLACE
            )
            close()
            onResult.invoke(result.toInt())
        }
    }

    fun removeAccount(id: Int, onSuccess: () -> Unit) {
        writableDatabase.apply {
            val count = delete(
                TABLE_ACCOUNTS,
                "$ROW_ID=?",
                arrayOf(id.toString())
            )
            close()
            if (count > 0) onSuccess.invoke()
        }
    }

    fun getAccounts(): MutableList<LocalAccount> {
        readableDatabase.apply {
            val accounts = mutableListOf<LocalAccount>()
            val cursor = query(
                TABLE_ACCOUNTS,
                arrayOf(
                    ROW_ID,
                    ACCOUNTS_COLUMN_INSTANCE,
                    ACCOUNTS_COLUMN_USERNAME,
                    ACCOUNTS_COLUMN_USER_URL,
                    ACCOUNTS_COLUMN_TOKEN,
                    ACCOUNTS_COLUMN_DISPLAY_NAME,
                    ACCOUNTS_COLUMN_AVATAR_URL
                ),
                null, null, null, null, ROW_ID
            )
            if (cursor.moveToFirst()) {
                do {
                    accounts.add(
                        LocalAccount(
                            id = cursor.getInt(cursor.getColumnIndexOrThrow(ROW_ID)),
                            instance = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_INSTANCE)),
                            username = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_USERNAME)),
                            userUrl = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_USER_URL)),
                            token = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_TOKEN)),
                            displayName = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_DISPLAY_NAME)),
                            avatarUrl = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_AVATAR_URL))
                        )
                    )
                } while (cursor.moveToNext())
            }
            cursor.close()
            close()
            return accounts
        }
    }

    fun getAccountById(id: Int): LocalAccount? {
        readableDatabase.apply {
            var account: LocalAccount? = null
            val cursor = query(
                TABLE_ACCOUNTS,
                arrayOf(
                    ROW_ID,
                    ACCOUNTS_COLUMN_INSTANCE,
                    ACCOUNTS_COLUMN_USERNAME,
                    ACCOUNTS_COLUMN_TOKEN,
                    ACCOUNTS_COLUMN_USER_URL,
                    ACCOUNTS_COLUMN_DISPLAY_NAME,
                    ACCOUNTS_COLUMN_AVATAR_URL
                ),
                "$ROW_ID=?",
                arrayOf(id.toString()),
                null, null, null
            )
            if (cursor.moveToFirst())
                account = LocalAccount(
                    id = cursor.getInt(cursor.getColumnIndexOrThrow(ROW_ID)),
                    instance = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_INSTANCE)),
                    username = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_USERNAME)),
                    userUrl = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_USER_URL)),
                    token = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_TOKEN)),
                    displayName = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_DISPLAY_NAME)),
                    avatarUrl = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_AVATAR_URL))
                )
            cursor.close()
            close()
            return account
        }
    }

    fun addTemplate(template: Template, onSuccess: (templateId: Int) -> Unit) {
        writableDatabase.apply {
            val result = insert(
                TABLE_TEMPLATES,
                null,
                ContentValues().apply {
                    put(TEMPLATES_COLUMN_NAME, template.name)
                    put(TEMPLATES_COLUMN_TEXT, template.text)
                    put(TEMPLATES_COLUMN_VISIBILITY, template.visibility.ordinal)
                    put(TEMPLATES_COLUMN_THREADING, template.threading.ordinal)
                    put(TEMPLATES_COLUMN_DATE, if (template.date) 1 else 0)
                    put(TEMPLATES_COLUMN_DATE_FORMAT, template.dateFormat)
                    put(TEMPLATES_COLUMN_LOCATION, if (template.location) 1 else 0)
                    put(TEMPLATES_COLUMN_LOCATION_FORMAT, template.locationFormat)
                })
            close()
            if (result > -1) onSuccess.invoke(result.toInt())
        }
    }

    fun updateTemplate(template: Template, onSuccess: () -> Unit) {
        writableDatabase.apply {
            val result = update(
                TABLE_TEMPLATES,
                ContentValues().apply {
                    put(TEMPLATES_COLUMN_NAME, template.name)
                    put(TEMPLATES_COLUMN_TEXT, template.text)
                    put(TEMPLATES_COLUMN_VISIBILITY, template.visibility.ordinal)
                    put(TEMPLATES_COLUMN_THREADING, template.threading.ordinal)
                    put(TEMPLATES_COLUMN_DATE, if (template.date) 1 else 0)
                    put(TEMPLATES_COLUMN_DATE_FORMAT, template.dateFormat)
                    put(TEMPLATES_COLUMN_LOCATION, if (template.location) 1 else 0)
                    put(TEMPLATES_COLUMN_LOCATION_FORMAT, template.locationFormat)
                },
                "$ROW_ID=?",
                arrayOf(template.id.toString())
            )
            close()
            if (result > -1) onSuccess.invoke()
        }
    }

    fun removeTemplate(id: Int, onSuccess: () -> Unit) {
        writableDatabase.apply {
            val count = delete(
                TABLE_TEMPLATES,
                "$ROW_ID=?",
                arrayOf(id.toString())
            )
            close()
            if (count > 0) onSuccess.invoke()
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getTemplates(): MutableList<Template> {
        readableDatabase.apply {
            val templates = mutableListOf<Template>()
            val cursor = query(
                TABLE_TEMPLATES,
                arrayOf(
                    ROW_ID,
                    TEMPLATES_COLUMN_NAME,
                    TEMPLATES_COLUMN_TEXT,
                    TEMPLATES_COLUMN_VISIBILITY,
                    TEMPLATES_COLUMN_THREADING,
                    TEMPLATES_COLUMN_DATE,
                    TEMPLATES_COLUMN_DATE_FORMAT,
                    TEMPLATES_COLUMN_LOCATION,
                    TEMPLATES_COLUMN_LOCATION_FORMAT
                ),
                null, null, null, null, ROW_ID
            )
            if (cursor.moveToFirst()) {
                do {
                    templates.add(
                        Template(
                            cursor.getInt(cursor.getColumnIndexOrThrow(ROW_ID)),
                            cursor.getString(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_NAME)),
                            cursor.getString(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_TEXT)),
                            Visibility.values()[cursor.getInt(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_VISIBILITY))],
                            Threading.values()[cursor.getInt(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_THREADING))],
                            cursor.getInt(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_DATE)) == 1,
                            cursor.getString(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_DATE_FORMAT)),
                            cursor.getInt(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_LOCATION)) == 1,
                            cursor.getString(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_LOCATION_FORMAT))
                        )
                    )
                } while (cursor.moveToNext())
            }
            cursor.close()
            close()
            return templates
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getTemplateById(id: Int): Template? {
        readableDatabase.apply {
            var template: Template? = null
            val cursor = query(
                TABLE_TEMPLATES,
                arrayOf(
                    ROW_ID,
                    TEMPLATES_COLUMN_NAME,
                    TEMPLATES_COLUMN_TEXT,
                    TEMPLATES_COLUMN_VISIBILITY,
                    TEMPLATES_COLUMN_THREADING,
                    TEMPLATES_COLUMN_DATE,
                    TEMPLATES_COLUMN_DATE_FORMAT,
                    TEMPLATES_COLUMN_LOCATION,
                    TEMPLATES_COLUMN_LOCATION_FORMAT
                ),
                "$ROW_ID=?",
                arrayOf(id.toString()),
                null, null, null
            )
            if (cursor.moveToFirst())
                template = Template(
                    cursor.getInt(cursor.getColumnIndexOrThrow(ROW_ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_NAME)),
                    cursor.getString(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_TEXT)),
                    Visibility.values()[cursor.getInt(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_VISIBILITY))],
                    Threading.values()[cursor.getInt(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_THREADING))],
                    cursor.getInt(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_DATE)) == 1,
                    cursor.getString(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_DATE_FORMAT)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_LOCATION)) == 1,
                    cursor.getString(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_LOCATION_FORMAT))
                )
            cursor.close()
            close()
            return template
        }
    }

    fun setThreadingId(accountId: Int, templateId: Int, statusId: String, statusDate: String) {
        writableDatabase.apply {
            insertWithOnConflict(
                TABLE_THREADING,
                null,
                ContentValues().apply {
                    put(THREADING_COLUMN_ACCOUNT_ID, accountId)
                    put(THREADING_COLUMN_TEMPLATE_ID, templateId)
                    put(THREADING_COLUMN_LAST_STATUS_ID, statusId)
                    put(THREADING_COLUMN_LAST_STATUS_DATE, statusDate)
                },
                SQLiteDatabase.CONFLICT_REPLACE
            )
            close()
        }
    }

    fun getThread(accountId: Int, templateId: Int): StatusThread {
        readableDatabase.apply {
            val thread = StatusThread(accountId, templateId, "", "")
            val cursor = query(
                TABLE_THREADING,
                arrayOf(
                    THREADING_COLUMN_LAST_STATUS_ID,
                    THREADING_COLUMN_LAST_STATUS_DATE
                ),
                "$THREADING_COLUMN_ACCOUNT_ID=? AND $THREADING_COLUMN_TEMPLATE_ID=?",
                arrayOf(
                    accountId.toString(),
                    templateId.toString()
                ),
                null, null, null, "1"
            )
            if (cursor.moveToFirst()) {
                thread.statusId = cursor.getString(cursor.getColumnIndexOrThrow(THREADING_COLUMN_LAST_STATUS_ID))
                thread.statusDate = cursor.getString(cursor.getColumnIndexOrThrow(THREADING_COLUMN_LAST_STATUS_DATE))
            }
            cursor.close()
            close()
            return thread
        }
    }

    fun clearThreading(templateId: Int) {
        writableDatabase.apply {
            delete(
                TABLE_THREADING,
                "$THREADING_COLUMN_TEMPLATE_ID=?",
                arrayOf(templateId.toString())
            )
            close()
        }
    }
}
