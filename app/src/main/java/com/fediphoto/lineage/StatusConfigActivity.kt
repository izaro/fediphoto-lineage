package com.fediphoto.lineage

import android.annotation.SuppressLint
import android.content.Context
import com.fediphoto.lineage.Helper.eiffelTowerLocation
import androidx.appcompat.app.AppCompatActivity
import com.fediphoto.lineage.datatypes.enums.Visibility
import com.fediphoto.lineage.datatypes.enums.Threading
import android.os.Bundle
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.fediphoto.lineage.databinding.ActivityStatusConfigBinding
import com.fediphoto.lineage.datatypes.Template
import java.lang.IllegalArgumentException
import java.text.SimpleDateFormat
import java.util.Date

class StatusConfigActivity : AppCompatActivity() {
    private val context: Context = this

    private lateinit var viewBinding: ActivityStatusConfigBinding
    private var isEditing = false

    private var templateId = -1
    private lateinit var prefs: Prefs
    private lateinit var database: Database

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityStatusConfigBinding.inflate(layoutInflater)
        prefs = Prefs(context)
        database = Database(context)

        intent.extras?.getInt(SELECTED_TEMPLATE_ID)?.let { selectedTemplateId ->
            database.getTemplateById(selectedTemplateId)?.let { template ->
                viewBinding.checkBoxStatusActive.isChecked = template.id == prefs.activeTemplateId

                viewBinding.radioVisibilityPublic.isChecked = Visibility.PUBLIC == template.visibility
                viewBinding.radioVisibilityUnlisted.isChecked = Visibility.UNLISTED == template.visibility
                viewBinding.radioVisibilityFollowers.isChecked = Visibility.PRIVATE == template.visibility
                viewBinding.radioVisibilityDirect.isChecked = Visibility.DIRECT == template.visibility

                viewBinding.radioThreadingAlways.isChecked = Threading.ALWAYS == template.threading
                viewBinding.radioThreadingDaily.isChecked = Threading.DAILY == template.threading
                viewBinding.radioThreadingNever.isChecked = Threading.NEVER == template.threading

                viewBinding.editTextLabel.setText(template.name)
                viewBinding.editTextText.setText(template.text)
                viewBinding.editTextDateFormat.setText(template.dateFormat)
                viewBinding.editTextGpsCoordinatesFormat.setText(template.locationFormat)

                templateId = template.id
                isEditing = true
            }
        }

        if (templateId == -1) {
            viewBinding.editTextDateFormat.setText(Helper.DEFAULT_DATE_FORMAT)
            viewBinding.editTextGpsCoordinatesFormat.setText(DEFAULT_GPS_COORDINATES_FORMAT)
            viewBinding.radioVisibilityUnlisted.isChecked = true
            viewBinding.radioThreadingDaily.isChecked = true
        }

        viewBinding.buttonClearThread.setOnClickListener {
            database.clearThreading(templateId)
            Log.i(TAG, "Threading ID cleared.")
            Toast.makeText(context, R.string.threading_id_cleared, Toast.LENGTH_LONG).show()
        }
        viewBinding.radioGroupThreading.setOnCheckedChangeListener { _, _ ->
            viewBinding.buttonClearThread.isEnabled = !viewBinding.radioThreadingNever.isChecked
        }

        viewBinding.buttonSave.setOnClickListener { save() }
        viewBinding.buttonCancel.setOnClickListener { finish() }
        setContentView(viewBinding.root)
    }

    private fun save() {
        Log.i(TAG, "save: begin")

        if (viewBinding.editTextLabel.text.toString().isBlank()) {
            Toast.makeText(context, R.string.label_can_not_be_blank, Toast.LENGTH_LONG).show()
            return
        }
        val label = viewBinding.editTextLabel.text.toString()
        val text = viewBinding.editTextText.text.toString()
        val date = viewBinding.editTextDateFormat.text.toString().isNotBlank()
        val dateFormat = viewBinding.editTextDateFormat.text.toString()
        val location = viewBinding.editTextGpsCoordinatesFormat.text.toString().isNotBlank()
        val locationFormat = viewBinding.editTextGpsCoordinatesFormat.text.toString()
        val visibility = when {
            viewBinding.radioVisibilityPublic.isChecked -> Visibility.PUBLIC
            viewBinding.radioVisibilityUnlisted.isChecked -> Visibility.UNLISTED
            viewBinding.radioVisibilityFollowers.isChecked -> Visibility.PRIVATE
            viewBinding.radioVisibilityDirect.isChecked -> Visibility.DIRECT
            else -> Visibility.PRIVATE
        }
        val threading = when {
            viewBinding.radioThreadingAlways.isChecked -> Threading.ALWAYS
            viewBinding.radioThreadingDaily.isChecked -> Threading.DAILY
            viewBinding.radioThreadingNever.isChecked -> Threading.NEVER
            else -> Threading.DAILY
        }
        val template = Template(templateId, label, text, visibility, threading, date, dateFormat, location, locationFormat)
        Database(context).let { database ->
            if (isEditing) database.updateTemplate(template) {
                Log.i(TAG, "updated template: $templateId")
            }
            else database.addTemplate(template) { newTemplateId ->
                templateId = newTemplateId
                Log.i(TAG, "saved template: $templateId")
            }
            val prefs = Prefs(context)
            if (viewBinding.checkBoxStatusActive.isChecked || prefs.activeTemplateId == -1)
                prefs.activeTemplateId = templateId
        }

        Log.i(TAG, "save: finish")
        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_status, menu)
        return true
    }

    @SuppressLint("SimpleDateFormat")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var intent = Intent(context, MainActivity::class.java)
        return when (item.itemId) {
            R.id.remove_status -> {
                Log.i(TAG, "Remove status config.")
                if (templateId == 0) {
                    Log.i(TAG, "No status to remove.")
                    Toast.makeText(context, R.string.no_status_config_to_remove, Toast.LENGTH_LONG).show()
                } else {
                    database.removeTemplate(templateId) {
                        Toast.makeText(context, R.string.status_config_removed_status_0_active, Toast.LENGTH_LONG).show()
                        database.getTemplates().let { templates ->
                            if (templates.isEmpty()) prefs.activeTemplateId = -1
                            else if (prefs.activeTemplateId == templateId) prefs.activeTemplateId = templates[0].id
                        }
                    }
                }
                setResult(RESULT_OK, intent)
                finish()
                true
            }
            R.id.restore_defaults -> {
                viewBinding.radioVisibilityPublic.isChecked = true
                viewBinding.radioThreadingDaily.isChecked = true
                viewBinding.editTextDateFormat.setText(Helper.DEFAULT_DATE_FORMAT)
                viewBinding.editTextGpsCoordinatesFormat.setText(DEFAULT_GPS_COORDINATES_FORMAT)
                true
            }
            R.id.test_date_format -> {
                val dateDisplay: String = try {
                    val sdf = SimpleDateFormat(viewBinding.editTextDateFormat.text.toString())
                    sdf.format(Date())
                } catch (e: IllegalArgumentException) {
                    getString(R.string.invalid_date_format)
                }
                Toast.makeText(context, dateDisplay, Toast.LENGTH_LONG).show()
                true
            }
            R.id.test_gps_coordinates_format -> {
                intent = Intent(Intent.ACTION_VIEW)
                if (viewBinding.editTextGpsCoordinatesFormat.text.toString().isNotBlank()) {
                    val urlString = String.format(
                        viewBinding.editTextGpsCoordinatesFormat.text.toString(),
                        eiffelTowerLocation.latitude,
                        eiffelTowerLocation.longitude
                    )
                    intent.data = Uri.parse(urlString)
                    startActivity(intent)
                }
                true
            }
            else -> {
                Log.i(TAG, "Default menu option.")
                super.onContextItemSelected(item)
            }
        }
    }

    companion object {
        private const val DEFAULT_GPS_COORDINATES_FORMAT = "https://www.openstreetmap.org/?mlat=%.5f&mlon=%.5f&zoom=17#layers=m"
        const val SELECTED_TEMPLATE_ID = "selected_template"
        private val TAG = this::class.java.canonicalName
    }
}
