package com.fediphoto.lineage

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.provider.MediaStore
import androidx.core.content.FileProvider
import android.widget.Toast
import android.os.Bundle
import android.os.Build
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.core.app.ActivityCompat
import kotlin.Throws
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.widget.EditText
import android.text.InputType
import android.content.DialogInterface
import android.widget.ArrayAdapter
import android.content.pm.ApplicationInfo
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.util.Patterns
import android.view.Menu
import android.view.MenuItem
import androidx.work.*
import com.fediphoto.lineage.databinding.ActivityMainBinding
import com.fediphoto.lineage.datatypes.Template
import java.io.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {
    private lateinit var viewBinding: ActivityMainBinding

    private var newApp: NewApp? = null

    private lateinit var photoFileName: String

    private lateinit var database: Database
    private lateinit var prefs: Prefs

    private fun dispatchTakePictureIntent() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(packageManager) != null) {
            try {
                val file = createPhotoFile()
                val photoUri = FileProvider.getUriForFile(this, "com.fediphoto.lineage.fileprovider", file)
                //Uri photoUri = Uri.fromFile(file);
                Log.i(TAG, String.format("photo URI: %s", photoUri.toString()))
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                startActivityForResult(intent, CAMERA_REQUEST)
            } catch (e: IOException) {
                Toast.makeText(this, e.localizedMessage, Toast.LENGTH_LONG).show()
                e.printStackTrace()
            }
        } else {
            Log.i(TAG, "Camera activity missing.")
        }
    }

    private fun setButtonCameraText() {
        val templateId = prefs.activeTemplateId
        val account = database.getAccountById(prefs.activeAccountId)
        val template = database.getTemplateById(templateId)
        if (account != null) {
            val username = "@" + account.username + "@" + account.instance
            var templateName = ""
            if (template != null) {
                templateName = template.name
            }
            val buttonCameraText = String.format(
                getString(R.string.button_camera_text),
                username,
                templateName
            )
            viewBinding.buttonCamera.text = buttonCameraText
        } else viewBinding.buttonCamera.text = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        database = Database(this)
        prefs = Prefs(this)

        viewBinding.buttonCamera.setOnClickListener {
            if (database.getAccounts().isEmpty()) {
                askForInstance()
                return@setOnClickListener
            }
            if (database.getTemplates().isEmpty()) {
                Toast.makeText(this, R.string.no_config, Toast.LENGTH_LONG).show()
                val intent = Intent(this, StatusConfigActivity::class.java)
                startActivityForResult(intent, REQUEST_STATUS)
                return@setOnClickListener
            }
            dispatchTakePictureIntent()
        }

        checkPermissionCamera()
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) checkPermissionStorage()

        if (database.getAccounts().isEmpty())
            askForInstance()
        else {
            setButtonCameraText()
            if (database.getTemplates().isEmpty())
                Toast.makeText(this, R.string.no_config, Toast.LENGTH_LONG).show()
        }

        Log.i(TAG, String.format("Camera on start setting: %s", prefs.cameraOnStart))
        if (prefs.cameraOnStart) dispatchTakePictureIntent()

        workerStatus(Literals.worker_tag_media_upload.name)
        workerStatus(Literals.worker_tag_post_status.name)

        setTitle()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSION_CAMERA -> {
                run {
                    if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                        Toast.makeText(this, R.string.camera_permission_granted, Toast.LENGTH_LONG).show()
                    else
                        Toast.makeText(this, R.string.camera_permission_denied, Toast.LENGTH_LONG).show()
                }
                run {
                    if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                        Toast.makeText(this, R.string.external_storage_permission_granted, Toast.LENGTH_LONG).show()
                    else
                        Toast.makeText(this, R.string.external_storage_permission_denied, Toast.LENGTH_LONG).show()
                }
            }
            REQUEST_PERMISSION_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this, R.string.external_storage_permission_granted, Toast.LENGTH_LONG).show()
                else
                    Toast.makeText(this, R.string.external_storage_permission_denied, Toast.LENGTH_LONG).show()
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun checkPermissionCamera() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA))
                Toast.makeText(this, R.string.need_camera_permission, Toast.LENGTH_LONG).show()
            else
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), REQUEST_PERMISSION_CAMERA)
        } else Log.i(TAG, "Camera permission already granted.")
    }

    private fun checkPermissionStorage() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                Toast.makeText(this, R.string.external_storage_permission_needed, Toast.LENGTH_LONG).show()
            else
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_PERMISSION_STORAGE)
        } else Log.i(TAG, "External storage permission already granted.")
    }

    @Throws(IOException::class)
    private fun createPhotoFile(): File {
        val timestamp = SimpleDateFormat("yyyyMMdd_HHmmss_", Locale.US).format(Date())
        val fileName = String.format("photo_%s", timestamp)
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if (storageDir == null || !storageDir.exists() && !storageDir.mkdir()) {
            Log.w(TAG, "Couldn't create photo folder.")
        }
        val file = File.createTempFile(fileName, ".jpg", storageDir)
        Log.i(TAG, String.format("Photo file: %s", file.absoluteFile))
        photoFileName = file.absolutePath
        return file
    }

    private fun submitWorkerUpload() {
        val file = File(photoFileName)
        Log.i(TAG, String.format("File %s exists %s", file.absoluteFile, file.exists()))
        @SuppressLint("RestrictedApi") val data = Data.Builder()
            .putString(Literals.fileName.name, file.absolutePath)
            .putInt(Literals.accountIndexSelected.name, prefs.activeAccountId)
            .putInt(Literals.statusIndexSelected.name, prefs.activeTemplateId)
            .build()
        val uploadWorkRequest = OneTimeWorkRequest.Builder(WorkerUpload::class.java)
            .addTag(Literals.worker_tag_media_upload.name)
            .addTag(
                String.format(
                    Locale.ENGLISH,
                    "%s%d",
                    Literals.created_milliseconds.name,
                    System.currentTimeMillis()
                )
            )
            .setInputData(data).build()
        var workContinuation = WorkManager.getInstance(this).beginWith(uploadWorkRequest)
        val postStatusWorkRequest = OneTimeWorkRequest.Builder(WorkerPostStatus::class.java)
            .addTag(Literals.worker_tag_post_status.name)
            .addTag(
                String.format(
                    Locale.ENGLISH,
                    "%s%d",
                    Literals.created_milliseconds.name,
                    System.currentTimeMillis()
                )
            )
            .setInputData(data).build()
        workContinuation = workContinuation.then(postStatusWorkRequest)
        workContinuation.enqueue()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        Log.i(TAG, String.format("Request code %d Result code %d", requestCode, resultCode))
        when (requestCode) {
            CAMERA_REQUEST -> {
                if (resultCode == RESULT_OK) {
                    Log.i(TAG, "Camera request returned OK.")
                    submitWorkerUpload()
                } else {
                    val file = File(photoFileName)
                    val fileDeleted = file.delete()
                    Log.i(TAG, String.format("File %s deleted %s", photoFileName, fileDeleted))
                }
            }
            TOKEN_REQUEST -> {
                if (resultCode == RESULT_OK) {
                    intent?.getStringExtra(Literals.authorization_code.name)?.let { authorizationCode ->
                        API().getAccessToken(
                            newApp!!.instanceDomain,
                            newApp!!.clientId,
                            newApp!!.clientSecret,
                            newApp!!.redirectUri,
                            authorizationCode
                        ) { accessToken ->
                            API().verifyToken(newApp!!.instanceDomain, accessToken) { instanceDomain, account ->
                                database.addAccount(account, instanceDomain, accessToken) { accountId ->
                                    Log.i(TAG, "onActivityResult: $accountId")
                                    prefs.activeAccountId = accountId
                                    newApp = null
                                    runOnUiThread { setButtonCameraText() }
                                    if (database.getTemplates().isEmpty()) {
                                        startActivityForResult(
                                            Intent(this@MainActivity, StatusConfigActivity::class.java),
                                            REQUEST_STATUS
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
            REQUEST_ACCOUNT -> {
                setButtonCameraText()
                setTitle()
                if (resultCode == REQUEST_ACCOUNT_RETURN) askForInstance()
            }
            REQUEST_STATUS -> setButtonCameraText()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    enum class Literals {
        client_name, access_token, POST, authorization_code, token, accounts, text, visibility, OK, Cancel, media_ids, id, status, url, fileName,
        accountIndexSelected, statusIndexSelected, worker_tag_media_upload, worker_tag_post_status, created_milliseconds, threading, in_reply_to_id
    }

    private fun askForInstance() {
        AlertDialog.Builder(this).apply {
            setTitle(R.string.enter_an_instance_name)
            val input = EditText(this@MainActivity).apply { inputType = InputType.TYPE_CLASS_TEXT }
            setView(input)
            setPositiveButton(Literals.OK.name) { _, _ ->
                val instance = input.text.toString().removePrefix("https://").removePrefix("http://")
                if (Patterns.WEB_URL.matcher(instance).matches()) {
                    Toast.makeText(this@MainActivity, String.format(getString(R.string.instance_string_format), instance), Toast.LENGTH_LONG).show()
                    API().createApp(instance) { redirectUri, clientId, clientSecret ->
                        newApp = NewApp(instance, redirectUri, clientId, clientSecret)
                        val intent = Intent(this@MainActivity, WebviewActivity::class.java)
                        intent.putExtra(
                            Helper.KEY_URL,
                            "https://$instance/oauth/authorize?scope=${Helper.REQUESTED_SCOPES}&response_type=code&redirect_uri=${newApp!!.redirectUri}&client_id=${newApp!!.clientId}"
                        )
                        startActivityForResult(intent, TOKEN_REQUEST)
                    }
                } else Toast.makeText(this@MainActivity, getString(R.string.invalid_domain), Toast.LENGTH_LONG).show()
            }
            setNegativeButton(Literals.Cancel.name) { dialog, _ -> dialog.cancel() }
            show()
        }
    }

    private fun multipleChoiceAccount() {
        AlertDialog.Builder(this).apply {
            setTitle(R.string.select_account)
            val adapter = ArrayAdapter<String>(this@MainActivity, android.R.layout.simple_list_item_1)
            database.getAccounts().let { accounts ->
                var index = 0
                for ((id, instance1, username1) in accounts) {
                    val checkMark = if (id == prefs.activeAccountId) CHECKMARK else ""
                    val username = "@$username1@$instance1"
                    adapter.add(String.format(Locale.US, "%s %d %s", checkMark, index++, username))
                }
                setAdapter(adapter) { _, which: Int ->
                    Log.i(TAG, "Selected instance: " + adapter.getItem(which))
                    val intent = Intent(this@MainActivity, AccountActivity::class.java)
                    intent.putExtra(AccountActivity.SELECTED_ACCOUNT_ID, accounts[which].id)
                    startActivityForResult(intent, REQUEST_ACCOUNT)
                }
                setNeutralButton(R.string.add_account) { _, _ -> askForInstance() }
                show()
            }
        }
    }

    private fun multipleChoiceStatuses() {
        AlertDialog.Builder(this).apply {
            setTitle(R.string.select_status)
            val adapter = ArrayAdapter<String>(this@MainActivity, android.R.layout.simple_list_item_1)
            val templates: List<Template> = database.getTemplates()
            var index = 0
            for ((id, name) in templates) {
                var checkmark = ""
                if (id == prefs.activeTemplateId) {
                    checkmark = CHECKMARK
                }
                adapter.add(String.format(Locale.US, "%s %d %s", checkmark, index++, name))
            }
            setAdapter(adapter) { _, which: Int ->
                val selectedItem = adapter.getItem(which)
                Log.i(TAG, "Selected status: $selectedItem")
                val intent = Intent(this@MainActivity, StatusConfigActivity::class.java)
                intent.putExtra(StatusConfigActivity.SELECTED_TEMPLATE_ID, templates[which].id)
                startActivityForResult(intent, REQUEST_STATUS)
            }
            setNeutralButton(R.string.add_status) { _, _ ->
                val intent = Intent(this@MainActivity, StatusConfigActivity::class.java)
                startActivityForResult(intent, REQUEST_STATUS)
            }
            show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.accounts -> {
                if (database.getAccounts().isEmpty()) askForInstance()
                else multipleChoiceAccount()
                Log.i(TAG, "Accounts activity")
                true
            }
            R.id.settings -> {
                Log.i(TAG, "Settings menu option.")
                startActivity(Intent(this, SettingsActivity::class.java))
                true
            }
            R.id.status_config -> {
                if (database.getTemplates().isEmpty())
                    startActivityForResult(Intent(this, StatusConfigActivity::class.java), REQUEST_STATUS)
                else
                    multipleChoiceStatuses()
                Log.i(TAG, "Statuses activity")
                true
            }
            R.id.about -> {
                startActivity(
                    Intent(Intent.ACTION_VIEW).apply { data = Uri.parse("https://silkevicious.codeberg.page/fediphoto-lineage.html") }
                )
                true
            }
            else -> {
                Log.i(TAG, "Default menu option.")
                super.onContextItemSelected(item)
            }
        }
    }

    private fun workerStatus(tag: String) {
        val workInfoList = WorkManager.getInstance(this).getWorkInfosByTag(tag)
        var quantityNotFinishedDayOrOlder = 0
        try {
            val workInfos = workInfoList[5, TimeUnit.SECONDS]
            var quantityNotFinished = 0
            val isDebuggable = 0 != applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE
            for (workInfo in workInfos) {
                if (workInfo != null && !workInfo.state.isFinished) {
                    var createdMilliseconds: Long = 0
                    val tags = workInfo.tags
                    for (workerTag in tags) {
                        Log.i(TAG, String.format("Worker tag: %s", workerTag))
                        if (workerTag.startsWith(Literals.created_milliseconds.name)) {
                            createdMilliseconds =
                                Utils.getLong(workerTag.substring(Literals.created_milliseconds.name.length))
                        }
                    }
                    Log.i(
                        TAG,
                        String.format(
                            "Worker created %s info %s. Worker Info Tag %s.",
                            Date(createdMilliseconds),
                            workInfo.toString(),
                            tag
                        )
                    )
                    quantityNotFinished++
                    if (createdMilliseconds > 0
                        && (System.currentTimeMillis() - createdMilliseconds > Utils.DAY_ONE
                                || isDebuggable && System.currentTimeMillis() - createdMilliseconds > Utils.MINUTES_ONE)
                    ) {
                        quantityNotFinishedDayOrOlder++
                    }
                }
            }
            Log.i(
                TAG,
                String.format(
                    "%d worker info quantity. Worker Info Tag %s. %d not finished.",
                    workInfos.size,
                    tag,
                    quantityNotFinished
                )
            )
        } catch (e: Exception) {
            Log.e(TAG, e.localizedMessage)
        }
        if (quantityNotFinishedDayOrOlder > 0) {
            promptUserToDeleteWorkers(quantityNotFinishedDayOrOlder)
        }
    }

    private fun promptUserToDeleteWorkers(quantity: Int) {
        val dialogClickListener = DialogInterface.OnClickListener { _, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    WorkManager.getInstance(this).cancelAllWork()
                    val message = String.format(Locale.getDefault(), getString(R.string.workers_cancelled), quantity)
                    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
                }
                DialogInterface.BUTTON_NEGATIVE -> {
                }
            }
        }
        val message = String.format(Locale.getDefault(), getString(R.string.older_workers_prompt), quantity)
        val builder = AlertDialog.Builder(this)
        builder.setMessage(message).setPositiveButton(R.string.yes, dialogClickListener)
            .setNegativeButton(R.string.no, dialogClickListener).show()
    }

    private fun setTitle() {
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setIcon(R.drawable.fediphoto_foreground)
        title = "${packageManager.getApplicationLabel(applicationInfo)} ${BuildConfig.VERSION_NAME}"
    }

    companion object {
        private data class NewApp(val instanceDomain: String, val redirectUri: String, val clientId: String, val clientSecret: String)

        private const val CAMERA_REQUEST = 169
        private const val TOKEN_REQUEST = 269
        private const val REQUEST_PERMISSION_CAMERA = 369
        private const val REQUEST_PERMISSION_STORAGE = 469
        private const val REQUEST_ACCOUNT = 569
        private const val REQUEST_STATUS = 769
        const val REQUEST_ACCOUNT_RETURN = 669
        private const val CHECKMARK = "✔"
        private const val TAG = "com.fediphoto.lineage.MainActivity"
    }
}
