package com.fediphoto.lineage

import android.content.Context
import android.content.SharedPreferences
import com.fediphoto.lineage.datatypes.enums.AfterPostAction

class Prefs(private val context: Context) {
    private val preferences: SharedPreferences =
        context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE)

    var activeAccountId: Int
        get() = preferences.getInt(keyActiveAccountId, -1)
        set(value) = preferences.edit().putInt(keyActiveAccountId, value).apply()

    var activeTemplateId: Int
        get() = preferences.getInt(keyActiveTemplateId, -1)
        set(value) = preferences.edit().putInt(keyActiveTemplateId, value).apply()

    var cameraOnStart: Boolean
        get() = preferences.getBoolean(keyCameraOnStart, false)
        set(value) = preferences.edit().putBoolean(keyCameraOnStart, value).apply()

    var afterPostAction: AfterPostAction
        get() = AfterPostAction.valueOf(preferences.getString(keyAfterPostAction, AfterPostAction.LEAVE.name)!!)
        set(value) = preferences.edit().putString(keyAfterPostAction, value.name).apply()

    private companion object {
        const val keyActiveAccountId = "active_account_id"
        const val keyActiveTemplateId = "active_template_id"
        const val keyCameraOnStart = "camera_on_start"
        const val keyAfterPostAction = "after_post_action"
    }
}
