package com.fediphoto.lineage

import android.app.Activity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import android.content.Intent
import android.util.Log

class WebviewActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
        val webView = findViewById<WebView>(R.id.webview)
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                Log.i(TAG, String.format("Page finished: %s", url))
                val returnUrl = "${Helper.RETURN_URL}?code="
                if (url.startsWith(returnUrl)) {
                    var token = url.substring(returnUrl.length)
                    if (token.endsWith("&state=")) {
                        token = token.substring(0, token.length - "&state=".length)
                    }
                    Log.i(TAG, String.format("Token \"%s\"", token))
                    setResult(RESULT_OK, Intent().apply { putExtra(MainActivity.Literals.authorization_code.name, token) })
                    finish()
                }
            }
        }
        intent.getStringExtra(Helper.KEY_URL)?.let { url -> webView.loadUrl(url) }
    }

    companion object {
        private val TAG = this::class.java.canonicalName
    }
}
